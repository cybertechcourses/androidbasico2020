package com.cybertech.testdiplo.models

import java.io.Serializable

data class Student(var id: Int = 0,
                   var name: String) : Serializable

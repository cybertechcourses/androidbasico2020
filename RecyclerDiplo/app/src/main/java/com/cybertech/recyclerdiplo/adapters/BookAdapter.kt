package com.cybertech.recyclerdiplo.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cybertech.recyclerdiplo.R
import com.cybertech.recyclerdiplo.adapters.viewholders.BookViewHolder
import com.cybertech.recyclerdiplo.listeners.BookListener
import com.cybertech.recyclerdiplo.models.Book

class BookAdapter(val books: List<Book>) : RecyclerView.Adapter<BookViewHolder>() {

    private var bookListener: BookListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_book, parent, false)
        val bookViewHolder = BookViewHolder(itemView)
        bookViewHolder.setBookListener(bookListener)
        return bookViewHolder
    }

    override fun onBindViewHolder(holder: BookViewHolder, position: Int) {
        holder.bindBook(books[position])
    }

    override fun getItemCount(): Int {
        return books.size
    }

    fun setBookListener(listener: BookListener?){
        this.bookListener=listener
    }
}
package com.cybertech.recyclerdiplo.adapters.viewholders

import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.cybertech.recyclerdiplo.R
import com.cybertech.recyclerdiplo.listeners.BookListener
import com.cybertech.recyclerdiplo.models.Book

class BookViewHolder(val itemView: View) : RecyclerView.ViewHolder(itemView) {

    private var bookListener:BookListener? = null
    private var book:Book? = null

    private var titleBookTextView: TextView = itemView.findViewById(R.id.titleBookTextView)
    private var pagesBookTextView: TextView = itemView.findViewById(R.id.pagesBookTextView)
    private var authorBookTextView: TextView = itemView.findViewById(R.id.authorBookTextView)
    private var publisherBookTextView: TextView = itemView.findViewById(R.id.publisherBookTextView)
    private var shareButton: Button = itemView.findViewById(R.id.shareButton)

    fun bindBook(book: Book) {
        this.book=book
        titleBookTextView.text = this.book?.title
        pagesBookTextView.text = this.book?.pages.toString()
        authorBookTextView.text = this.book?.author
        publisherBookTextView.text = this.book?.publisher

        itemView.setOnClickListener { view ->
            bookListener?.onClickBook(this.book!!)
        }
        shareButton.setOnClickListener { view ->
            bookListener?.onShareBook(this.book!!)
        }
    }

    fun setBookListener(listener: BookListener?){
        this.bookListener=listener
    }
}
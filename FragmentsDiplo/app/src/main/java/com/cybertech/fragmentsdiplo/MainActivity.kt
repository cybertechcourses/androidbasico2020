package com.cybertech.fragmentsdiplo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.cybertech.fragmentsdiplo.fragments.MainFragment
import com.cybertech.fragmentsdiplo.listeners.DetailBookListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), DetailBookListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.mainContainer,MainFragment.newInstance())
            .commit()

    }

    override fun onShowTitle(title: String) {
        supportActionBar?.apply {
            this.title=title
        }
    }
}
package com.cybertech.fragmentsdiplo.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.cybertech.fragmentsdiplo.R
import com.cybertech.fragmentsdiplo.listeners.DetailBookListener
import com.cybertech.recyclerdiplo.models.Book
import kotlinx.android.synthetic.main.fragment_detail_book.*
import java.lang.Exception

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_BOOK = "book"

/**
 * A simple [Fragment] subclass.
 * Use the [DetailBookFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DetailBookFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var book: Book? = null
    private var detailBookListener: DetailBookListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is DetailBookListener) {
            detailBookListener = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            book = it.getParcelable(ARG_BOOK)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_book, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        book?.let {
            detailBookListener?.onShowTitle(it.title)
            titleBookDetailTextView.text = it.title
            authorBookDetailTextView.text = it.author
            publisherBookDetailTextView.text = it.publisher
        }
    }

    companion object {

        @JvmStatic
        fun newInstance(book: Book) =
            DetailBookFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_BOOK, book)
                }
            }
    }
}
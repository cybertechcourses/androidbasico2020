package com.cybertech.fragmentsdiplo.listeners

interface DetailBookListener {

    fun onShowTitle(title:String)
}
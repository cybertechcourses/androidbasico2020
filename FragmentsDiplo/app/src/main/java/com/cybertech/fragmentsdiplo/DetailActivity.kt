package com.cybertech.fragmentsdiplo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.cybertech.fragmentsdiplo.fragments.DetailBookFragment
import com.cybertech.fragmentsdiplo.listeners.DetailBookListener
import com.cybertech.recyclerdiplo.models.Book
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() , DetailBookListener{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        setSupportActionBar(detailToolbar)

        val book = intent.getParcelableExtra<Book>("book")

        book?.let {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.detailContainer,DetailBookFragment.newInstance(it))
                .commit()
        }

    }

    override fun onShowTitle(title: String) {
        supportActionBar?.apply {
            this.title=title
        }
    }
}
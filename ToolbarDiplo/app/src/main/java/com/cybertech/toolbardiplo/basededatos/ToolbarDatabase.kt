package com.cybertech.toolbardiplo.basededatos

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.cybertech.recyclerdiplo.models.Book
import com.cybertech.toolbardiplo.TypeConnection

class ToolbarDatabase(val context: Context,
                      val nameBD: String,
                      val typeConnection: TypeConnection) {

    var toolbarSQLiteDatabase: SQLiteDatabase? = null

    init {
        val toolbarDatabaseConnection = ToolbarDatabaseConnection(context, nameBD, null, 1)
        toolbarSQLiteDatabase = when (typeConnection) {
            TypeConnection.READONLY -> {
                toolbarDatabaseConnection.readableDatabase
            }
            TypeConnection.WRITABLE -> {
                toolbarDatabaseConnection.writableDatabase
            }
        }
    }

    fun saveBookDatabase(book: Book): Boolean {
        var insertBook: Long = 0
        val bookContentValues = ContentValues()
        bookContentValues.put("title", book.title)
        bookContentValues.put("publisher", book.publisher)
        bookContentValues.put("author", book.author)
        bookContentValues.put("pages", book.pages)
        toolbarSQLiteDatabase?.let {
            insertBook = it.insert("Books",
                null, bookContentValues)
        }
        return insertBook > 0
    }

    fun updateBookDatabase(book: Book): Boolean {
        var updateBook: Int = 0
        val bookContentValues = ContentValues()
        bookContentValues.put("title", book.title)
        bookContentValues.put("publisher", book.publisher)
        bookContentValues.put("author", book.author)
        bookContentValues.put("pages", book.pages)
        val args = arrayOf(book.id.toString())
        val clause = "id=?"
        toolbarSQLiteDatabase?.let {
            updateBook = it.update("Books", bookContentValues, clause, args)
        }
        return updateBook > 0
    }

    fun deleteBookDatabase(book: Book): Boolean {
        var deleteBook = 0
        val args = arrayOf(book.id.toString())
        val clause = "id=?"
        toolbarSQLiteDatabase?.let {
            deleteBook = it.delete("Books", clause, args)
        }
        return deleteBook > 0
    }

    fun obtainBookDatabase(id: Int): Book? {
        val fields = arrayOf("id", "title", "publisher", "author", "pages")
        val args = arrayOf(id.toString())
        val clause = "id=?"
        toolbarSQLiteDatabase?.let {
            val cursor = it.query("Books", fields, clause, args, null, null, null)
            if (cursor.moveToFirst()) {
                do {
                    return Book(cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getInt(3),
                        cursor.getString(4))
                } while (cursor.moveToNext())
            }
        }
        return null
    }

    fun getBooks(): List<Book> {
        val books = arrayListOf<Book>()
        val fields = arrayOf("id", "title", "publisher", "author", "pages")
        toolbarSQLiteDatabase?.let {
            val cursor = it.query("Books", fields, null, null, null, null, null)
            if (cursor.moveToFirst()) {
                do {
                    books.add(Book(cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getInt(3),
                        cursor.getString(4)))
                } while (cursor.moveToNext())
            }
        }
        return books
    }
}
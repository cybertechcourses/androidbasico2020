package com.cybertech.toolbardiplo.basededatos

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class ToolbarDatabaseConnection(context: Context,
                                nameBD:String,
                                cursorFactory: SQLiteDatabase.CursorFactory?,
                                version:Int): SQLiteOpenHelper(context,nameBD,cursorFactory,version) {

    val bookCreate = "CREATE TABLE Books (id INTEGER  PRIMARY KEY AUTOINCREMENT, " +
        "title TEXT, " +
        "publisher TEXT, " +
        "author TEXT, " +
        "pages INTEGER );"
    override fun onCreate(db: SQLiteDatabase?) {
        db?.let {
            it.execSQL(bookCreate)
        }
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.let {
            it.execSQL("DROP TABLE IF EXISTS Books")
            it.execSQL(bookCreate)
        }
    }
}
package com.cybertech.toolbardiplo

enum class TypeConnection {
    READONLY,
    WRITABLE
}
package com.cybertech.recyclerdiplo.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cybertech.recyclerdiplo.adapters.viewholders.BookViewHolder
import com.cybertech.recyclerdiplo.listeners.BookListener
import com.cybertech.recyclerdiplo.models.Book
import com.cybertech.toolbardiplo.R


class BookAdapter(val books: List<Book>) : RecyclerView.Adapter<BookViewHolder>() {

    private val booksNew: ArrayList<Book> = arrayListOf()
    private var bookListener: BookListener? = null

    init {
        if (books.isNotEmpty()) {
            booksNew.addAll(books)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_book, parent, false)
        val bookViewHolder = BookViewHolder(itemView)
        bookViewHolder.setBookListener(bookListener)
        return bookViewHolder
    }

    override fun onBindViewHolder(holder: BookViewHolder, position: Int) {
        holder.bindBook(booksNew[position])
    }

    override fun getItemCount(): Int {
        return booksNew.size
    }

    fun updateBook(book: Book) {
        if(!booksNew.isNullOrEmpty()) {
            booksNew.add(book)
            notifyDataSetChanged()
        }
    }

    fun cleanBooks() {
        booksNew.clear()
        notifyDataSetChanged()
    }

    fun updateBook(book: Book, position: Int) {
        if (booksNew.isNotEmpty() && position < booksNew.size) {
            booksNew.add(position, book)
            notifyItemInserted(position)
        }
    }

    fun setBookListener(listener: BookListener?) {
        this.bookListener = listener
    }
}
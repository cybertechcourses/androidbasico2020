package com.cybertech.toolbardiplo

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.cybertech.recyclerdiplo.models.Book
import com.cybertech.toolbardiplo.MainActivity.Companion.RESULT_ERROR_REGISTER_BOOK
import com.cybertech.toolbardiplo.MainActivity.Companion.RESULT_REGISTER_BOOK
import com.cybertech.toolbardiplo.basededatos.ToolbarDatabase
import kotlinx.android.synthetic.main.activity_register_book.*

class RegisterBookActivity : AppCompatActivity() {
    //private var diploPreferences: SharedPreferences? = null

    private var toolbarDatabase: ToolbarDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_book)
        setSupportActionBar(registerBookToolbar)

        toolbarDatabase= ToolbarDatabase(baseContext,"booksBD",TypeConnection.WRITABLE)
        //diploPreferences = getSharedPreferences("DiploPreferences", Context.MODE_PRIVATE)

        supportActionBar?.apply {
            title = "Registro de libro"
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }

        saveBookButton.setOnClickListener {
            val title = titleTextInputLayout.editText?.text.toString()
            val publisher = publisherTextInputLayout.editText?.text.toString()
            val author = authorTextInputLayout.editText?.text.toString()
            val pages = pagesTextInputLayout.editText?.text.toString()
            if (title.isNotEmpty() && pages.isNotEmpty()) {
                val id = title.length * 5
                val newBook = Book(id, title, publisher, pages.toInt(), author)
                toolbarDatabase?.saveBookDatabase(newBook)
                val bookIntent = Intent(this, MainActivity::class.java).apply {
                    val bookBundle = Bundle().apply {
                        putParcelable("book", newBook)
                    }
                    putExtras(bookBundle)
                }
                setResult(RESULT_REGISTER_BOOK, bookIntent)
            } else {
                setResult(RESULT_ERROR_REGISTER_BOOK)
            }
            finish()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
/*
    private fun saveBook(book: Book) {
        diploPreferences?.let {
            val editor = it.edit()
            editor.putInt("id", book.id)
            editor.putString("title", book.title)
            editor.putString("publisher", book.publisher)
            editor.putString("author", book.author)
            editor.putInt("pages", book.pages)
            editor.commit()
        }
    }

    private fun getBookSaved(): Book {
        return diploPreferences?.let {
            val title = it.getString("title", "")
            val publisher = it.getString("publisher", "")
            val author = it.getString("author", "")
            title?.let { it1 ->
                publisher?.let { it2 ->
                    author?.let { it3 ->
                        Book(
                            it.getInt("id", 0),
                            it1,
                            it2,
                            it.getInt("pages", 0),
                            it3)
                    }
                }
            }
        } ?: Book()

    }*/
}